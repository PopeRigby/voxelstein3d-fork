Compiling.
==========

I recommend you use the following:
1. Visual Studio Express 2005
2. Platform SDK
3. Microsoft Macro Assembler 8.0
4. DirectX SDK

All these are free downloads.

I have included libvorbis and libogg as compiled libraries, if you
use another compiler you will probably need to compile them yourself.

Good luck.
